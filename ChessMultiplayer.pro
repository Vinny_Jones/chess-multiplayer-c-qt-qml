#	Chess multiplayer by Andrii Pyvovar

#	Features:
#		Full movement logic for every figure
#		Next possible move highlight
#		Check, checkmate(win), draw indication
#		Castling
#		En passant
#		Current game status in status bar
#		Beaten figures at side menu
#		Pawn exchange on any figure (when pawn reach last row)
#		Game save. Current game stores is user "Documents" folder.
#		Load saved game. Input file validation

#		"History" mode - previous/next/first/last move.
#		Continue game at any position in history
#		"Readible" history file (E2:E4)


TEMPLATE = app

QT += qml quick widgets

CONFIG += c++11
CONFIG += CONSOLE

SOURCES += main.cpp \
	figures_management.cpp \
    moveLogic.cpp \
    gameLogic.cpp

HEADERS += logic.h \
	figures_management.h

RESOURCES += qml.qrc
