#include "logic.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char *argv[])
{
	QGuiApplication app(argc, argv);

//	GameLogic logic;

	QQmlApplicationEngine engine;

	GameLogic logic;

	engine.rootContext()->setContextProperty("logic", &logic);
	engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

	if (engine.rootObjects().isEmpty())
		return -1;
	return app.exec();
}
