#ifndef FIGURES_H
#define FIGURES_H

#include <QPoint>
#include <QList>

/* Holds figure parameters */
struct	Figure {
	bool			color;			/* Figure color (from "Color" enum) */
	int				type;			/* Figure type (from "Figure_type" enum) */
	int				X;				/* Current X coordinate */
	int				Y;				/* Current Y coordinate */
	int				move_mask;		/* Mask of possible movement according to "Move_maks" */
	int				made_moves;		/* Number of moves, already made by figure */
	bool			beaten;			/* True - if figure is beaten */
	QList<QPoint>	moves;			/* List of possible cells for next move */
};

enum	Color {
	White = 0,
	Black = 1,
};

enum	Type {
	Pawn,
	Rook,
	Knight,
	Bishop,
	Queen,
	King
};

/* Every figure has it's own "move mask" - Bit mask indicating possible moves */
enum	MoveMask {
	multi_cell = 0b1,			/* Set for figures, which can move on distance, more, than one cell */
	front_back = 0b10,			/* Set for figures, which can move front and back */
	left_right = 0b1000,		/* Set for figures, which can move left and right */
	diagonal = 0b10000,			/* Set for figures, which can move by diagonal */
	knight = 0b100000,			/* Special move for knight (regardless of othes options) */
	pawn = 0b1000000,			/* Special move/beat mask for pawn (regardless of othes options) */
};

/* Class, responsible for creating and placing figures on game board */
class	figures_management
{
public:
	figures_management(int boardCells);
	~figures_management(void);

	QList<Figure>	initiate(void);
	Figure			createFigure(bool color, int type, int cellX, int cellY);

private:
	int		boardCells;
};

#endif /* FIGURES_H */
