#include "figures_management.h"

figures_management::figures_management(int cells) : boardCells(cells) {
}

figures_management::~figures_management(void) {
}

/* Create figures on their initial positions */
QList<Figure>	figures_management::initiate(void)
{
	QList<Figure>	figures;

	figures << createFigure(Color::White, Type::King, 4, 7);
	figures << createFigure(Color::Black, Type::King, 4, 0);
	figures << createFigure(Color::White, Type::Queen, 3, 7);
	figures << createFigure(Color::Black, Type::Queen, 3, 0);
	for (int i = 0; i < boardCells; i++)
	{
		figures << createFigure(Color::White, Type::Pawn, i, 6);
		figures << createFigure(Color::Black, Type::Pawn, i, 1);
		if (i == 0 || i == 7)
		{
			figures << createFigure(Color::White, Type::Rook, i, 7);
			figures << createFigure(Color::Black, Type::Rook, i, 0);
		}
		else if (i == 1 || i == 6)
		{
			figures << createFigure(Color::White, Type::Knight, i, 7);
			figures << createFigure(Color::Black, Type::Knight, i, 0);
		}
		else if (i == 2 || i == 5)
		{
			figures << createFigure(Color::White, Type::Bishop, i, 7);
			figures << createFigure(Color::Black, Type::Bishop, i, 0);
		}
	}
	return (figures);
}

/* Creates figure with appropriate movement mask */
Figure	figures_management::createFigure(bool color, int type, int cellX, int cellY)
{
	int		figure_mask;

	if (type == Type::Pawn)
		figure_mask = MoveMask::pawn;
	else if (type == Type::Knight)
		figure_mask = MoveMask::knight;
	else if (type == Type::Rook)
		figure_mask = MoveMask::multi_cell | MoveMask::front_back | MoveMask::left_right;
	else if (type == Type::Bishop)
		figure_mask = MoveMask::multi_cell | MoveMask::diagonal;
	else if (type == Type::Queen)
		figure_mask = MoveMask::multi_cell | MoveMask::front_back | MoveMask::left_right | MoveMask::diagonal;
	else
		figure_mask = MoveMask::front_back | MoveMask::left_right | MoveMask::diagonal;

	return (Figure {color, type, cellX, cellY, figure_mask, 0, 0, QList<QPoint>()});

}
