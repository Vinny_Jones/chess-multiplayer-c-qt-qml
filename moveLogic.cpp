#include "logic.h"

MoveLogic::MoveLogic(QObject *parent) : QAbstractListModel(parent), FiguresManagement(BOARD_CELLS)
{
	pawnExchangeTypes << "Queen";
	pawnExchangeTypes << "Bishop";
	pawnExchangeTypes << "Rook";
	pawnExchangeTypes << "Knight";
	pawnExchangeTypes << "Pawn";
}

MoveLogic::~MoveLogic() {
}

int MoveLogic::board_cells() const {
	return (BOARD_CELLS);
}

QVariant MoveLogic::data(const QModelIndex &modelIndex, int role) const
{
	if (!modelIndex.isValid()) {
		return QVariant();
	}

	int index = modelIndex.row();
	if (index >= figures.size() || index < 0) {
		return QVariant();
	}

	switch (role) {
	case Common_attributes::color:		return figures[index].color;
	case Common_attributes::type:		return figures[index].type;
	case Common_attributes::X:			return figures[index].X;
	case Common_attributes::Y:			return figures[index].Y;
	case Common_attributes::beaten:		return figures[index].beaten;
	}
	return QVariant();
}

int     MoveLogic::rowCount(const QModelIndex &) const {
  return (figures.size());
}


QHash<int, QByteArray>	MoveLogic::roleNames() const
{
	QHash<int, QByteArray> attributes;

	attributes.insert(Common_attributes::color,		"figure_color");
	attributes.insert(Common_attributes::type,		"figure_type");
	attributes.insert(Common_attributes::X,			"figure_X");
	attributes.insert(Common_attributes::Y,			"figure_Y");
	attributes.insert(Common_attributes::beaten,	"figure_beaten");

  return attributes;
}

/* Starting game with default parameters */
void    MoveLogic::startGame(void)
{
	beginResetModel();
	figures = FiguresManagement.initiate();
	checkStatus = -1;
	currentPlayer = Color::White;
	emit setCurrentPlayer(currentPlayer);
	updateMoves();
	endResetModel();
}

/* Set of functions to find every possible move for every remaining figure on board */
void	MoveLogic::updateMoves(void)
{
	int				i;

	for (i = 0; i < figures.size(); i++)
	{
		figures[i].moves.clear();
		if (figures[i].beaten)
			continue ;
		if (figures[i].type == Type::Pawn)
			updatePawnMoves(figures[i]);
		else if (figures[i].type == Type::Knight)
			updateKnightMoves(figures[i]);
		else
			updateMovesByMask(figures[i]);
	}
	castlingCheck();
	enPassantPoint.setX(-1);
	updateGameStatus();
}

/* Check if it possible to make castling */
void	MoveLogic::castlingCheck(void)
{
	if (checkStatus == currentPlayer || figures[currentPlayer].made_moves)		/* White king at figures[0], black - at figures[1] */
		return ;
	int cellY = (currentPlayer) ? 0 : BOARD_CELLS - 1;
	Figure	*rook;
	if ((rook = findFigure(0, cellY)) && rook->type == Type::Rook && !rook->made_moves)
		if (!findFigure(1, cellY) && !findFigure(2, cellY) && !findFigure(3, cellY))
			if(validateMove(figures[currentPlayer], 3, cellY))
			{
				figures[currentPlayer].moves.pop_back();
				validateMove(figures[currentPlayer], 2, cellY);
			}
	if ((rook = findFigure(7, cellY)) && rook->type == Type::Rook && !rook->made_moves)
		if (!findFigure(5, cellY) && !findFigure(6, cellY))
			if (validateMove(figures[currentPlayer], 5, cellY))
			{
				figures[currentPlayer].moves.pop_back();
				validateMove(figures[currentPlayer], 6, cellY);
			}
}

/* Run castling */
void	MoveLogic::castlingMove(Figure *king, int toX, int toY)
{
	if (king->type != Type::King || abs(toX - king->X) <= 1)
		return ;
	int rookIndex = findFigureIndex((toX - king->X < 0) ? 0 : BOARD_CELLS - 1, toY);
	figures[rookIndex].X = (toX - king->X < 0) ? 3 : 5;
	emit dataChanged(createIndex(rookIndex, 0), createIndex(rookIndex, 0));
}


/* Find all possible moves for figure except pawn and knight (their special moves handled by special functions) */
/* Possible moves are found according to "Movement mask": it stores directions and distance, which figure can move */
void	MoveLogic::updateMovesByMask(Figure &figure)
{
	int		limit = (figure.move_mask & MoveMask::multi_cell) ? BOARD_CELLS - 1 : 1;
	bool	up, down, left, right, upright, upleft, downright, downleft;

	up = down = (figure.move_mask & MoveMask::front_back) ? true : false;
	right = left = (figure.move_mask & MoveMask::left_right) ? true : false;
	upright = upleft = downright = downleft = (figure.move_mask & MoveMask::diagonal) ? true : false;

	for (int i = 1; i <= limit; i++)
	{
		if (up)
			up = checkMove(figure, figure.X, figure.Y - i);
		if (down)
			down = checkMove(figure, figure.X, figure.Y + i);
		if (left)
			left = checkMove(figure, figure.X - i, figure.Y);
		if (right)
			right = checkMove(figure, figure.X + i, figure.Y);
		if (upright)
			upright = checkMove(figure, figure.X + i, figure.Y - i);
		if (upleft)
			upleft = checkMove(figure, figure.X - i, figure.Y - i);
		if (downright)
			downright = checkMove(figure, figure.X + i, figure.Y + i);
		if (downleft)
			downleft = checkMove(figure, figure.X - i, figure.Y + i);
		if (!(up | down | left | right | upright | upleft | downright | downleft))
			break ;
	}
}

/* Returns true if movement accepted. */
/* In case of false (out of boad line or other figure found) serching in given direction stops */
bool	MoveLogic::checkMove(Figure &figure, int cellX, int cellY)
{
	if (!VALID_CELL(cellX, cellY))
		return (false);
	Figure	*found = findFigure(cellX, cellY);

	if (!found)
	{
		validateMove(figure, cellX, cellY);
		return (true);
	}
	else if (figure.color != found->color)
	{
		if (found->type == Type::King)
			setCheck(found->color);
		else
			validateMove(figure, cellX, cellY);
	}
	return (false);
}

/* Find all possible moves for pawn */
void	MoveLogic::updatePawnMoves(Figure &pawn)
{
	Figure	*found;
	int		direction = (pawn.color == Color::White) ? -1 : 1;
	int		cellY;

	cellY = (1 * direction) + pawn.Y;
	if ((found = findFigure(pawn.X - 1, cellY)) && found->color != pawn.color)
	{
		if (found->type == Type::King)
			setCheck(found->color);
		else
			validateMove(pawn, pawn.X - 1, cellY);
	}
	if ((found = findFigure(pawn.X + 1, cellY)) && found->color != pawn.color)
	{
		if (found->type == Type::King)
			setCheck(found->color);
		else
			validateMove(pawn, pawn.X + 1, cellY);
	}
	enPassantCheck(pawn, cellY);

	if (!VALID_CELL(pawn.X, cellY) || findFigure(pawn.X, cellY))
		return ;
	validateMove(pawn, pawn.X, cellY);
	cellY = (2 * direction) + pawn.Y;
	if (!pawn.made_moves && !findFigure(pawn.X, cellY))
		validateMove(pawn, pawn.X, cellY);
}

/* En Passant move check */
void	MoveLogic::enPassantCheck(Figure &pawn, int cellY)
{
	if (enPassantPoint.isNull() || enPassantPoint.rx() < 0)
		return ;
	if (pawn.X - 1 == enPassantPoint.rx() && cellY == enPassantPoint.ry())
		validateMove(pawn, pawn.X - 1, cellY);
	if (pawn.X + 1 == enPassantPoint.rx() && cellY == enPassantPoint.ry())
		validateMove(pawn, pawn.X + 1, cellY);
}

/* En Passant point and move management */
void	MoveLogic::enPassantMove(Figure *pawn, int toX, int toY)
{
	if (pawn->type != Type::Pawn)
		return ;
	if (pawn->X == toX && abs(pawn->Y - toY) == 2)
		enPassantPoint = QPoint(toX, (toY > pawn->Y) ? pawn->Y + 1 : pawn->Y - 1);				/* Setting enPassant point */
	else if (pawn->X != toX && !findFigure(toX, toY))
	{
		int beatenIndex = findFigureIndex(toX, pawn->Y);
		if (beatenIndex < 0)
			return ;
		setBeaten(beatenIndex);
	}
}

/* Find all possible moves for knight */
void	MoveLogic::updateKnightMoves(Figure &knight)
{
	Figure	*found;
	int		cellX, cellY;

	for (int i = -2; i <= 2; i++)
		for (int j = -2; j <= 2; j++)
			if (abs(i) + abs(j) == 3)
			{
				cellX = knight.X + i;
				cellY = knight.Y + j;
				if ((found = findFigure(cellX, cellY)))
				{
					if (found->color != knight.color)
					{
						if (found->type == Type::King)
							setCheck(found->color);
						else
							validateMove(knight, cellX, cellY);
					}
				}
				else if (VALID_CELL(cellX, cellY))
					validateMove(knight, cellX, cellY);
			}
}

/* In case of found valid move we have to also ensure, that it wont lead to check on king */
/* Following functon validates move by checking if players king is in safe position */
bool	MoveLogic::validateMove(Figure &figure, int cellX, int cellY)
{
	int	copyX = figure.X;
	int	copyY = figure.Y;
	Figure	*dest = findFigure(cellX, cellY);
	Figure	&king = figures[figure.color];
	bool	result = true;
	int		up, down, left, right, upright, upleft, downright, downleft, i;

	up = down = left = right = upright = upleft = downright = downleft = 1;
	if (dest)
		dest->beaten = true;
	figure.X = cellX;
	figure.Y = cellY;
	if (!isCheckPawnKnight(king))
		result = false;
	for (i = 1; result && i <= BOARD_CELLS - 1; i++)
	{
		if (up > 0)
			up = isCheck(king.color, king.X, king.Y - i, MoveMask::front_back, i);
		if (down > 0)
			down = isCheck(king.color, king.X, king.Y + i, MoveMask::front_back, i);
		if (left > 0)
			left = isCheck(king.color, king.X - i, king.Y, MoveMask::left_right, i);
		if (right > 0)
			right = isCheck(king.color, king.X + i, king.Y, MoveMask::left_right, i);
		if (upright > 0)
			upright = isCheck(king.color, king.X + i, king.Y - i, MoveMask::diagonal, i);
		if (upleft > 0)
			upleft = isCheck(king.color, king.X - i, king.Y - i, MoveMask::diagonal, i);
		if (downright > 0)
			downright = isCheck(king.color, king.X + i, king.Y + i, MoveMask::diagonal, i);
		if (downleft > 0)
			downleft = isCheck(king.color, king.X - i, king.Y + i, MoveMask::diagonal, i);
		if (!up || !down || !left || !right || !upright || !upleft || !downright || !downleft)
			result = false;
	}
	if (dest)
		dest->beaten = false;
	figure.X = copyX;
	figure.Y = copyY;
	if (result)
		figure.moves << QPoint(cellX, cellY);
	return (result);
}

/* If king is under check by oppenent's figures (except pawn and knight - handled by another functions) */
int		MoveLogic::isCheck(bool color, int cellX, int cellY, int mask, int distance)
{
	if (!VALID_CELL(cellX, cellY))
		return (-1);
	Figure	*temp = findFigure(cellX, cellY);
	if (!temp)
		return (1);
	if (temp->color == color)
		return (-1);
	if (temp->move_mask	& mask)
		if (temp->move_mask & MoveMask::multi_cell || distance == 1)
			return (0);
	return (-1);
}

/* If king is under check by oppenent's figures pawn or knight */
bool	MoveLogic::isCheckPawnKnight(Figure &king)
{
	int		i;
	Figure	*temp;

	i = (king.color == Color::White) ? -1 : 1;
	if (VALID_CELL(king.X - 1, king.Y + i) && (temp = findFigure(king.X - 1, king.Y + i)))
		if (temp->type == Type::Pawn && temp->color != king.color)
			return (false);
	if (VALID_CELL(king.X + 1, king.Y + i) && (temp = findFigure(king.X + 1, king.Y + i)))
		if (temp->type == Type::Pawn && temp->color != king.color)
			return (false);
	for (i = -2; i <= 2; i++)
		for (int j = -2; j <= 2; j++)
			if (abs(i) + abs(j) == 3)
			{
				if (VALID_CELL(king.X + i, king.Y + j) && (temp = findFigure(king.X + i, king.Y + j)))
					if (temp->type == Type::Knight && temp->color != king.color)
						return (false);
			}
	return (true);
}

/* Set check status */
void	MoveLogic::setCheck(bool color)
{
	if (color == Color::White)
		checkStatus = Color::White;
	else
		checkStatus = Color::Black;
}

/* Check coordinates and make move */
bool	MoveLogic::makeMove(int fromX, int fromY, int toX, int toY)
{
	Figure	*figure;

	if (!VALID_CELL(toX, toY) || (fromX == toX && fromY == toY) || !(figure = findFigure(fromX, fromY)))
		return (false);
	for (int i = 0; i < figure->moves.size(); i++)
	{
		if (figure->moves[i].rx() == toX && figure->moves[i].ry() == toY)
		{
			int beatenIndex = findFigureIndex(toX, toY);
			int	figureIndex = findFigureIndex(fromX, fromY);

			castlingMove(figure, toX, toY);
			enPassantMove(figure, toX, toY);
			figure->X = toX;
			figure->Y = toY;
			figure->made_moves++;
			setBeaten(beatenIndex);
			currentPlayer = (currentPlayer == Color::White) ? Color::Black : Color::White;
			if (figure->type == Type::Pawn && (figure->Y == 0 || figure->Y == BOARD_CELLS - 1))
				emit exchangePawn(figure->color, figure->X, figure->Y);
			updateMoves();
			emit dataChanged(createIndex(figureIndex, 0), createIndex(figureIndex, 0));
			return (true);
		}
	}
	return (false);
}

/* Sets figure "beaten" flag as true. Meaning figure has to be removed from board */
void	MoveLogic::setBeaten(int index)
{
	if (index >= 0)
	{
		figures[index].beaten = true;
		emit dataChanged(createIndex(index, 0), createIndex(index, 0));
	}
}

/* Update game status - win, lose, draw or normal (game continues) */
void	MoveLogic::updateGameStatus(void)
{
	bool	currentCheck = (currentPlayer == checkStatus) ? true : false;
	QString	loseStatus = (currentPlayer == Color::White) ? "Black won !" : "White won !";

	if (!findMoves(currentPlayer))
		emit changeStatus((currentCheck) ? loseStatus : "Draw");
	else if (currentCheck)
		emit changeStatus((currentPlayer == Color::White) ? "Check on white king !" : "Check on black king !");
	else
		emit changeStatus(" ");

	checkStatus = -1;
}

/* Check if player has at least one move to make */
bool	MoveLogic::findMoves(bool color)
{
	for (int i = 0; i < figures.size(); i++)
		if (figures[i].color == color && !figures[i].moves.empty())
			return (true);
	return (false);
}

/* Slot for highlighting next possible move */
bool	MoveLogic::nextMoveCheck(int fromX, int fromY, int cellX, int cellY)
{
	Figure	*figure;

	if (!VALID_CELL(fromX, fromY) || !VALID_CELL(cellX, cellY) || !(figure = findFigure(fromX, fromY)))
		return (false);
	for (int i = 0; i < figure->moves.size(); i++)
		if (figure->moves[i].rx() == cellX && figure->moves[i].ry() == cellY)
			return (true);
	return (false);
}

/* For pawn exchage operations */
QStringList	MoveLogic::exchangeTypes(void) {
	return (pawnExchangeTypes);
}

/* Logic of pawn exchange onto figure, picked by player */
bool	MoveLogic::runExchangePawn(bool color, QString type, int cellX, int cellY)
{
	int	pawnIndex = findFigureIndex(cellX, cellY);

	if (pawnIndex < 0 || figures[pawnIndex].type != Type::Pawn || figures[pawnIndex].color != color)
		return (false);
	if (type == "Queen")
		figures[pawnIndex] = FiguresManagement.createFigure(color, Type::Queen, cellX, cellY);
	else if (type == "Bishop")
		figures[pawnIndex] = FiguresManagement.createFigure(color, Type::Bishop, cellX, cellY);
	else if (type == "Rook")
		figures[pawnIndex] = FiguresManagement.createFigure(color, Type::Rook, cellX, cellY);
	else if (type == "Knight")
		figures[pawnIndex] = FiguresManagement.createFigure(color, Type::Knight, cellX, cellY);
	else
		figures[pawnIndex] = FiguresManagement.createFigure(color, Type::Pawn, cellX, cellY);
	emit dataChanged(createIndex(pawnIndex, 0), createIndex(pawnIndex, 0));
	return (true);
}

/* Find figure. Return pointer or index */
Figure	*MoveLogic::findFigure(int x, int y)
{
	if (!VALID_CELL(x, y))
		return (NULL);
	for (int i = 0; i < figures.size(); i++)
		if (figures[i].X == x && figures[i].Y == y && !figures[i].beaten)
			return (&figures[i]);
	return (NULL);
}

int		MoveLogic::findFigureIndex(int x, int y)
{
	if (!VALID_CELL(x, y))
		return (-1);
	for (int i = 0; i < figures.size(); i++)
		if (figures[i].X == x && figures[i].Y == y && !figures[i].beaten)
			return (i);
	return (-1);
}
