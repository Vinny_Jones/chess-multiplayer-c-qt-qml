import QtQuick 2.0
import QtQuick.Controls 1.3

/* Custom made button */
Rectangle {
    id:custombutton
    width: 150
    height: 80
    border.color: "white"
    radius: 10

    property string title: "Button"
    property int text_size: 18

    Text
    {
        id: buttonLabel
        text: title
        font.pointSize: text_size
        anchors.centerIn: parent
        color: "black"
        font.bold: true
    }

    property color buttonColor: "lightblue"
    property color onHoverColor: "gold"
    property color borderColor: "white"

    MouseArea {
        id: buttonMouseArea
        anchors.fill: parent
        onClicked: buttonClick()
        hoverEnabled: true
        onEntered: parent.border.color = parent.onHoverColor
        onExited: parent.border.color = parent.borderColor
    }
    function buttonClick() {}
    color: buttonMouseArea.pressed ? "#ffce9e" : buttonColor
    Behavior on color { ColorAnimation { duration: 55 } }

    scale: buttonMouseArea.pressed ? 1.1 : 1.0
    Behavior on scale { NumberAnimation{ duration: 55} }
}
