import QtQuick 2.6
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2

Rectangle {
    id:historyModeMenu
    width: menuListView.width
    height: 115
    color: "lightblue"
    radius: 5

    Row {
        id:firstRow
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        spacing: 10

        CustomButton {
            id: loadHistory
            title: "Load"
            width: 85
            height: 50
            text_size: 15

            function buttonClick() { selectFileDialog.visible = true; }
        }

        CustomButton {
            id: prevButton
            title: "Prev <<"
            width: 85
            height: 50
            text_size: 15

            function buttonClick() { logic.goTo(-1); }
        }

        CustomButton {
            id:nextButton
            title: "Next >>"
            width: 85
            height: 50
            text_size: 15

            function buttonClick() { logic.goTo(1); }
        }

        FileDialog {
            id: selectFileDialog
            title: qsTr("Select a file")
            selectMultiple: false
            selectFolder: false
            nameFilters: [ "Text files (*.txt)" ]
            selectedNameFilter: "Text files (*.txt)"
            onAccepted: logic.loadHistoryFile(fileUrl.toString().replace("file://", ""))
        }
    }


    Row {
        id:secondRow
        anchors.top: firstRow.bottom
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        spacing: 10

        CustomButton {
            id: continueFromCurrentButton
            title: "Continue<br/>from here"
            width: 85
            height: 50
            text_size: 15

            function buttonClick() {
                logic.continueGame()
                menuListView.currentIndex = 0
            }
        }

        CustomButton {
            id: toStartButton
            title: "To begin"
            width: 85
            height: 50
            text_size: 15

            function buttonClick() {
                logic.goToMove(0);
            }
        }

        CustomButton {
            id:toEndButton
            title: "To end"
            width: 85
            height: 50
            text_size: 15

            function buttonClick() {
                logic.goToMove(-1);
            }
        }
    }
}
