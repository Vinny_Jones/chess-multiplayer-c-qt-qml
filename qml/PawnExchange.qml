import QtQuick 2.6
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.0

Rectangle {
    id:exchangePawnDialog
    anchors.fill: parent
    z:4
    color: Qt.rgba(1,1,1,0.5)
    visible: false
    MouseArea { anchors.fill: parent }

    /* Trash values. Will be changed on function call */
    property bool pawnExchangeColor: true
    property int pawnExchangeX: 0
    property int pawnExchangeY: 0

    Rectangle {
        anchors.centerIn: parent
        width: 300
        height: 200
        color: "lightblue"
        radius: 20
        z:555


        ComboBox {
            id: combobox
            anchors.top: parent.top
            anchors.topMargin: 20
            anchors.left: parent.left
            anchors.leftMargin: 100

            width: 100
            height: 50
            model: logic.exchangeTypes
            currentIndex: 0

            style: ComboBoxStyle {
                font {
                    pixelSize: 20
                    bold:true
                }
                background: Rectangle {
                    id: rectCategory
                    radius: 5
                    border.width: 2
                    color: "#fff"
                }
            }
        }

        CustomButton {
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            anchors.left: parent.left
            anchors.leftMargin: 50
            title: "Select"
            width: 200
            height: 100
            buttonColor: "gold"
            function buttonClick() {
                if (logic.runExchangePawn(pawnExchangeColor, combobox.currentText, pawnExchangeX, pawnExchangeY))
                    logic.writePawnChange(combobox.currentText);
                exchangePawnDialog.visible = false
            }
        }
    }
    Connections {
        target: logic
        onExchangePawn: {
            exchangePawnDialog.visible = true;
            pawnExchangeColor = pawncolor;
            pawnExchangeX = cellX;
            pawnExchangeY = cellY;
        }
    }
}
