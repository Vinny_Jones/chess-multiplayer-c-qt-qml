import QtQuick 2.6
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.0
import QtGraphicalEffects 1.0

Rectangle {
    id: layout_board
    anchors.fill: parent

    property int cellSize: layout_board.width / boardSize   /* Stores current cell size */
    property int figureSize: cellSize * 0.7                 /* Stores current figure (image) size */

    onWidthChanged: {
        cellSize = layout_board.width / boardSize           /* Resizing game board in case application window resized. */
        figureSize = cellSize * 0.7                         /* Changing figures size and positions accordingly         */
        for (var i = 0; i < figures.count; i++)
            figures.itemAt(i).relocate()
    }

    Rectangle {
        id: board_layout
        anchors.fill: parent
        color: "transparent"
        z:1

        Repeater {
            id:figures
            model: logic
            property int currentPlayer: 0                           /* Player: 0 for white figures, 1 - black */

            Image {
                property int figureColor: figure_color              /* Black or white figure */
                property int figureType: figure_type                /* Figure type */
                property bool isBeaten: figure_beaten               /* If the figure is beaten, it has to be hidden */
                property int cellX: figure_X                        /* X cell coordinate */
                property int cellY: figure_Y                        /* Y cell coordinate */

                id:figureImage
                z:2
                source: "/images/" + images[figureType][figureColor]
                width: figureSize
                height: width

                onIsBeatenChanged: {
                    z = (isBeaten) ? 1 : 2;
                    visible = (isBeaten) ? false : true
                }
                onCellXChanged: relocate()
                onCellYChanged: relocate()

                MouseArea {
                    id: figure_area
                    anchors.fill: parent
                    drag.target: (figures.currentPlayer == figureColor) ? parent : undefined

                    property int fromX: 0
                    property int fromY: 0
                    property int to_X: 0
                    property int to_Y: 0

                    onPressed: {
                        parent.z = 3
                        fromX = parent.x / cellSize;
                        fromY = parent.y / cellSize;
                        if (figures.currentPlayer == figureColor || figures.currentPlayer == -1)
                            highlight_move(fromX, fromY)
                    }
                    onReleased: {
                        parent.z = 2
                        if (figures.currentPlayer == figureColor || figures.currentPlayer == -1)
                            clear_highlight()

                        if (parent.x >= 0 && parent.x <= board_layout.width && parent.y >= 0 && parent.y <= board_layout.height)
                        {
                            to_X = ((parent.x + width / 2) / cellSize);
                            to_Y = ((parent.y + height / 2) / cellSize);

                            if (logic.makeMove(fromX, fromY, to_X, to_Y))
                            {
                                logic.writeMove(fromX, fromY, to_X, to_Y)
                                figures.currentPlayer = (figures.currentPlayer) ? 0 : 1         /* Switch active player */
                            }
                        }
                        parent.relocate();
                    }
                }

                Behavior on x { NumberAnimation { duration: 200 } }
                Behavior on y { NumberAnimation { duration: 200 } }
                Behavior on visible { NumberAnimation { duration: 300} }

                Component.onCompleted: relocate()
                function relocate()
                {
                    x =  cellX * cellSize + (cellSize - width) / 2
                    y =  cellY * cellSize + (cellSize - height) / 2
                }
            }
        }
    }

    Image {
        id: image_board
        anchors.fill: parent
        width: parent.width
        height: parent.height
        source: "/images/board.png"
    }

    function highlight_move(fromX, fromY)
    {
        for (var i = 0; i < highlight_circles.count; i++)
            if (logic.nextMoveCheck(fromX, fromY, parseInt(i % boardSize), parseInt(i / boardSize)))
                highlight_circles.itemAt(i).visible = true
    }
    function clear_highlight()
    {
        for (var i = 0; i < highlight_circles.count; i++)
            highlight_circles.itemAt(i).visible = false
    }


    Repeater {
        model: boardSize * boardSize
        id:highlight_circles

        Image {
            source: "/images/circle.png"
            z:0
            visible: false

            property int xCell: index % boardSize
            property int yCell: index / boardSize

            x:cellSize * xCell
            y:cellSize * yCell
            width: cellSize
            height: cellSize
        }
    }
    Connections {
        target: logic
        onSetCurrentPlayer: figures.currentPlayer = player
    }
}
