import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2

ApplicationWindow {

    title: qsTr("Chess Multiplayer")
    visible: true
    width: 1160
    height: 860
    onWidthChanged: height = width - 300                    /* Right menu always 300 */
    onHeightChanged: height = width - 300                   /* Height changes only on width changed */
    minimumWidth: 800
    maximumWidth: 2300
    property int boardSize: logic.board_cells               /* Stores current game board size (cells) */

    /* Filenames of fugure pictues */
    property variant images: [
        ["white_pawn.svg", "black_pawn.svg"],
        ["white_rook.svg", "black_rook.svg"],
        ["white_knight.svg", "black_knight.svg"],
        ["white_bishop.svg", "black_bishop.svg"],
        ["white_queen.svg", "black_queen.svg"],
        ["white_king.svg", "black_king.svg"],
    ]
    property variant layoutItems: [ "A", "B", "C", "D", "E", "F", "G", "H" ]

    Rectangle {
        id: layoutBoard
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.height
        height: width
        color: "#9f5232"

        /* Game board */
        LayoutBoard {}
    }

    /* Layout of menu at right side of screen */
    Rectangle {
        id:layuot_menu
        anchors.top: parent.top
        anchors.left: layoutBoard.right
        anchors.bottom: parent.bottom
        width: parent.width - layoutBoard.width
        color: "transparent"

        Image {
            anchors.top: parent.top
            anchors.left: parent.left
            source: "/images/wood.jpg"
        }

        /* Main buttons - "Play", "History" and "Exit" */
        Rectangle {
            id:controlButtons
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            width: parent.width - 10
            height: 100
            color: "lightblue"
            radius: 5
            z:1

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                spacing: 10

                CustomButton {
                    id: gameMode;
                    title: "Play"
                    width: 85
                    height: 50

                    function buttonClick() { menuListView.currentIndex = 0; }
                }

                CustomButton {
                    id: historyMode
                    title: "History"
                    width: 85
                    height: 50

                    function buttonClick() { menuListView.currentIndex = 1; }
                }

                CustomButton {
                    id:exitButton
                    width: 85
                    height: 50
                    title: "Exit"

                    function buttonClick() { dialogOnExit.open(); }
                    MessageDialog {
                        id:dialogOnExit
                        title: "Exiting..."
                        icon: StandardIcon.Question
                        text: "Exit game?"
                        standardButtons: StandardButton.Yes | StandardButton.No
                        onYes: Qt.quit()
                    }
                }
            }
        }

        /* Submenus for "Play" and "History" */
        Rectangle {
            id:subMenu
            anchors.top: controlButtons.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 5
            width: parent.width - 10
            height: controlButtons.height
            color: "transparent"

            VisualItemModel {
                id: menuListModel

                GameMenu {}
                HistoryMenu {}
            }

            ListView {
                id: menuListView
                model: menuListModel
                anchors.fill: parent
                spacing: 30

                orientation: ListView.Vertical
                highlightMoveDuration:200
            }
        }

        /* Beaten figures placing */
        BeatenHub {}

        /* Status bar (at bottom of menu) */
        Rectangle {
            id:statusBar
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            width: parent.width - 10
            height: 30
            color: "lightblue"
            radius: 5

            Text {
                id: statusBarText
                anchors.centerIn: parent
                text: qsTr("Status bar")
                font.family: "Arial"
                font.pointSize: 18
                color: "brown"
            }
        }

        Connections {
            target: logic
            onChangeStatus: statusBarText.text = status
        }
    }

    /* Dialog for pawn exchange */
    PawnExchange {
        id:exchangePawnDialog
    }
}
