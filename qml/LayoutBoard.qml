import QtQuick 2.6

/* Layout screen with rectangles + repeaters for letters & numbers */
/* Game board inserted from file "GameBoard" */
Rectangle {
    anchors.fill: parent
    color: "transparent"

    /* Top rectangle (for top letters) */
    Rectangle
    {
        id:topMid
        anchors.top:parent.top
        anchors.left: parent.left
        anchors.leftMargin: 30
        width: parent.width - 60
        height: 30
        color:"transparent"

        Row {
            anchors.fill: parent
            Repeater {
                model: boardSize
                Rectangle {
                    width: parent.width / boardSize
                    height: parent.height
                    color: "transparent"
                    Text {
                        anchors.centerIn: parent;
                        text: layoutItems[index]
                        color: "white"
                        font.bold: true
                        font.pixelSize: 15
                    }
                }
            }
        }
    }

    /* Left rectangle (for left numbers) */
    Rectangle {
        id:leftMid
        anchors.top:parent.top
        anchors.topMargin: 30
        anchors.left: parent.left
        width: 30
        height: parent.height - 60
        color: "transparent"

        Column {
            anchors.fill: parent
            Repeater {
                model: boardSize
                Rectangle {
                    width: parent.width
                    height: parent.height / boardSize
                    color: "transparent"
                    Text {
                        anchors.centerIn: parent;
                        text: boardSize - index
                        color: "white"
                        font.bold: true
                        font.pixelSize: 15
                    }
                }
            }
        }
    }

    /* Mid rectangle. For game board */
    Rectangle {
        id:mid
        anchors.top: topMid.bottom
        anchors.left: leftMid.right
        width: parent.width - 60
        height: parent.height - 60

        GameBoard {}
    }

    /* Right rectangele (for right numbers) */
    Rectangle {
        id:righMid
        anchors.top: mid.top
        anchors.right: parent.right
        width: 30
        height: mid.height
        color: "transparent"

        Column {
            anchors.fill: parent
            Repeater {
                model: boardSize
                Rectangle {
                    width: parent.width
                    height: parent.height / boardSize
                    color: "transparent"
                    Text {
                        anchors.centerIn: parent;
                        text: boardSize - index
                        color: "white"
                        font.bold: true
                        font.pixelSize: 15
                    }
                }
            }
        }
    }

    /* Bottom rectangle (for bottom letters) */
    Rectangle {
        id:bottomMid
        anchors.top: mid.bottom
        anchors.left: mid.left
        width: mid.width
        height: 30
        color: "transparent"

        Row {
            anchors.fill: parent
            Repeater {
                model: boardSize
                Rectangle {
                    width: parent.width / boardSize
                    height: parent.height
                    color: "transparent"
                    Text {
                        anchors.centerIn: parent;
                        text: layoutItems[index]
                        color: "white"
                        font.bold: true
                        font.pixelSize: 15
                    }
                }
            }
        }
    }
}
