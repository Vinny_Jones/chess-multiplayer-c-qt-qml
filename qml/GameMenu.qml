import QtQuick 2.6
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2

Rectangle {
    id: gameModeMenu
    width: menuListView.width
    height: 115
    color: "lightblue"
    radius: 5

    Row {
        id:firstRow
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 35
        spacing: 20

        CustomButton {
            id: startButton
            title: "Start"
            width: 100
            height: 50
            text_size: 15

            function buttonClick() {
                logic.newGame()
            }
        }

        CustomButton {
            id: stopButton
            title: "Stop"
            width: 100
            height: 50
            text_size: 15

            function buttonClick() {
                if (!logic.isEmpty())
                    dialogOnStop.visible = true
                else
                    statusBarText.text = qsTr("Game board is already clear")
            }
            MessageDialog {
                id:dialogOnStop
                title: "Stop game"
                icon: StandardIcon.Question
                text: "Unsaved game will be lost. Continue?"
                standardButtons: StandardButton.Yes | StandardButton.No
                onYes: {
                    logic.clearBoard()
                    statusBarText.text = qsTr("Game board cleared")
                }
            }
        }
    }

    Row {
        id:secondRow
        anchors.top: firstRow.bottom
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 35
        spacing: 20

        CustomButton {
            id:saveButton
            title: "Save"
            width: 100
            height: 50
            text_size: 15

            function buttonClick() {
                if (!logic.isEmpty())
                    saveFileDialog.visible = true
                else
                    statusBarText.text = qsTr("Can't save empty game")
            }
        }

        FileDialog {
            id: saveFileDialog
            title: qsTr("Save current game")
            selectMultiple: false
            selectFolder: false
            selectExisting: false
            folder: shortcuts.home
            nameFilters: [ "Text files (*.txt)" ]
            selectedNameFilter: "Text files (*.txt)"
            onAccepted: logic.saveGame(fileUrl.toString().replace("file://", ""))
        }

        CustomButton {
            id: loadSavedGame
            title: "Load"
            width: 100
            height: 50
            text_size: 15

            function buttonClick() {
                selectGameDialog.visible = true
            }
        }

        FileDialog {
            id: selectGameDialog
            title: qsTr("Select saved game file")
            selectMultiple: false
            selectFolder: false
            nameFilters: [ "Text files (*.txt)" ]
            selectedNameFilter: "Text files (*.txt)"
            onAccepted: logic.loadGame(fileUrl.toString().replace("file://", ""))
        }
    }
}
