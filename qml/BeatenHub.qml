import QtQuick 2.6
import QtQuick.Controls 1.3

/* Beaten figures zone */
Rectangle {
    id:beatenMenu
    anchors.top: subMenu.bottom
    anchors.topMargin: 30
    anchors.left: parent.left
    anchors.leftMargin: 5
    width: parent.width - 10
    height: 200
    radius: 20
    color: "lightblue"

    Label {
        id:labelText
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("Beaten figures")
        font.family: "Arial"
        font.pointSize: 25
        color: "#d18b47"
    }

    property int imgSize: beatenPlace.width / 8
    /* Fixed coordinates */
    property variant indexImg: [
        [4 * imgSize, 3 * imgSize],
        [4 * imgSize, 0],
        [3 * imgSize, 3 * imgSize],
        [3 * imgSize, 0],
        [0 * imgSize, 2 * imgSize],
        [0 * imgSize, 1 * imgSize],
        [0 * imgSize, 3 * imgSize],
        [0 * imgSize, 0],
        [1 * imgSize, 2 * imgSize],
        [1 * imgSize, 1 * imgSize],
        [1 * imgSize, 3 * imgSize],
        [1 * imgSize, 0],
        [2 * imgSize, 2 * imgSize],
        [2 * imgSize, 1 * imgSize],
        [2 * imgSize, 3 * imgSize],
        [2 * imgSize, 0],
        [3 * imgSize, 2 * imgSize],
        [3 * imgSize, 1 * imgSize],
        [4 * imgSize, 2 * imgSize],
        [4 * imgSize, 1 * imgSize],
        [5 * imgSize, 2 * imgSize],
        [5 * imgSize, 1 * imgSize],
        [5 * imgSize, 3 * imgSize],
        [5 * imgSize, 0],
        [6 * imgSize, 2 * imgSize],
        [6 * imgSize, 1 * imgSize],
        [6 * imgSize, 3 * imgSize],
        [6 * imgSize, 0],
        [7 * imgSize, 2 * imgSize],
        [7 * imgSize, 1 * imgSize],
        [7 * imgSize, 3 * imgSize],
        [7 * imgSize, 0],
    ]

    Rectangle {
        id:beatenPlace
        anchors.top:labelText.bottom
        anchors.left: parent.left
        anchors.leftMargin: 10
        color: "transparent"
        width: parent.width - 20
        height: parent.height - 50


        Repeater {
            id:beatenFigures
            model: logic

            Image {
                property int figureColor: figure_color              /* Black or white figure */
                property int figureType: figure_type                /* Figure type */
                property bool isBeaten: figure_beaten               /* If the figure is beaten, it has to be hidden */

                source: "/images/" + images[figureType][figureColor]
                width: beatenPlace.imgSize
                visible: false
                onIsBeatenChanged: visible = (figure_beaten) ? true : false

                Component.onCompleted: relocate()
                function relocate()
                {
                    x =  indexImg[index][0];
                    y =  indexImg[index][1];
                }
            }
        }
    }
}
