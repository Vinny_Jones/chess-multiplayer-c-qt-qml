#ifndef LOGIC_H
#define LOGIC_H

#include <QAbstractListModel>
#include <QPoint>
#include <QTextStream>
#include <QDir>
#include <QStandardPaths>
#include <QFile>
#include "figures_management.h"

#define BOARD_CELLS 8
#define VALID_CELL(X, Y) ((X) >= 0 && (X) < BOARD_CELLS && (Y) >= 0 && (Y) < BOARD_CELLS)
#define CURRENTGAMEFILE "chessMultiplayer_current.txt"

/* Attributes, needed for UI (QML) side */
enum	Common_attributes {
	color = Qt::UserRole,
	type,
	X,
	Y,
	beaten,
};

class MoveLogic : public QAbstractListModel
{
	Q_OBJECT
	Q_PROPERTY(int board_cells READ board_cells CONSTANT)
	Q_PROPERTY(QStringList exchangeTypes READ exchangeTypes CONSTANT)


public:
	MoveLogic(QObject *parent = 0);
	~MoveLogic();
	int						board_cells(void) const;
	QStringList				exchangeTypes(void);

signals:
	void					changeStatus(QString status);
	void					exchangePawn(bool pawncolor, int cellX, int cellY);
	void					setCurrentPlayer(int player);

public slots:
	void					startGame();
	bool					makeMove(int fromX, int fromY, int toX, int toY);
	bool					nextMoveCheck(int fromX, int fromY, int cellX, int cellY);
	bool					runExchangePawn(bool color, QString type, int cellX, int cellY);

protected:
	int						rowCount(const QModelIndex & parent) const override;
	QVariant				data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
	QHash<int, QByteArray>	roleNames() const override;
	void					updateMoves();
	void					enPassantMove(Figure *pawn, int toX, int toY);
	void					updateGameStatus(void);
	Figure					*findFigure(int x, int y);
	int						findFigureIndex(int x, int y);

	bool					currentPlayer;
	QList<Figure>			figures;
	QStringList				pawnExchangeTypes;
	QPoint					enPassantPoint;

private:
	figures_management		FiguresManagement;
	void					enPassantCheck(Figure &pawn, int cellY);
	void					updatePawnMoves(Figure &pawn);
	void					updateKnightMoves(Figure &knight);
	void					updateMovesByMask(Figure &figure);
	void					castlingCheck();
	void					castlingMove(Figure *king, int toX, int toY);
	bool					checkMove(Figure &figure, int cellX, int cellY);
	bool					validateMove(Figure &figure, int cellX, int cellY);
	int						isCheck(bool color, int cellX, int cellY, int mask, int distance);
	bool					isCheckPawnKnight(Figure &king);
	bool					findMoves(bool color);
	void					setBeaten(int index);
	void					setCheck(bool color);

	int						checkStatus;
};


class GameLogic : public MoveLogic
{
	Q_OBJECT
public:
	GameLogic(void);
	~GameLogic(void);

signals:
	void			moveFigure(int index, int toX, int toY);

public slots:
	void			newGame(void);
	void			writeMove(int fromX, int fromY, int toX, int toY);
	void			writePawnChange(QString exchangeTo);
	bool			loadHistoryFile(QString filename);
	void			loadGame(QString filename);
	void			continueGame(void);
	void			saveGame(QString filename);
	void			clearBoard(void);
	bool			isEmpty(void);
	void			goToMove(int moveIndex);
	void			goTo(int delta);

private:
	int						currentIndex;
	bool					parseInputString(QString line);
	bool					openFile(QString filename, QFile::OpenMode mode);
	bool					loadHistory(QString filename);
	void					checkEnPassantRule(void);
	QString					currentFilePath;
	QFile					file;
	QList<QList<Figure>>	fullHistory;
	QList<QPoint>			enPassantHistory;
};

#endif /* LOGIC_H */
