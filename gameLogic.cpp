#include "logic.h"

GameLogic::GameLogic(void) {
	openFile(CURRENTGAMEFILE, QIODevice::ReadWrite | QIODevice::Text);
	currentFilePath = file.fileName();
}

GameLogic::~GameLogic() {
}

/* Loading history file line by line, call line validation function */
bool	GameLogic::loadHistory(QString filename)
{
	openFile(filename, QIODevice::ReadWrite | QIODevice::Text);
	startGame();
	fullHistory.clear();
	fullHistory << figures;
	if(file.atEnd())
	{
		changeStatus("Input file is empty");
		return (false);
	}
	this->blockSignals(true);
	while (!file.atEnd()) {
		if(!parseInputString(file.readLine()))
		{
			this->blockSignals(false);
			emit changeStatus("Invalid input file");
			return (false);
		}
		fullHistory << figures;
	}
	if (file.fileName() != currentFilePath)
		if (!QFile::remove(CURRENTGAMEFILE) || !QFile::copy(file.fileName(), CURRENTGAMEFILE))
			emit changeStatus("File error. Start from point will fail");
	this->blockSignals(false);
	figures = fullHistory[0];
	currentIndex = 0;
	emit setCurrentPlayer(-1);
	return (true);
}

/* Parse/validate input string - check it's validity. Check pawn exchange */
bool	GameLogic::parseInputString(QString line)
{
	int			fromX, fromY, toX, toY;

	QStringList split = line.split(':');
	if (split[0].size() != 2 || split[1].size() != 3)
		return (false);
	fromX = split[0].toLatin1()[0] - 65;
	fromY = abs(split[0].toLatin1()[1] - 48 - BOARD_CELLS);
	toX = split[1].toLatin1()[0] - 65;
	toY = abs(split[1].toLatin1()[1] - 48 - BOARD_CELLS);
	if (!makeMove(fromX, fromY, toX, toY))
		return (false);
	Figure	*target = findFigure(toX, toY);
	if ((toY == 7 || toY == 0) && target->type == Type::Pawn)
	{
		line = file.readLine();
		split = line.split(':');
		if (split[0] != "Pawn" || !pawnExchangeTypes.contains(split[1].remove('\n')))
			return (false);
		if (!runExchangePawn(target->color, split[1], toX, toY))
			return (false);
	}
	return (true);
}

/* Going to some point at game history */
void	GameLogic::goTo(int delta)
{
	goToMove((currentIndex + delta < 0) ? 0 : currentIndex + delta);
}

void	GameLogic::goToMove(int moveIndex)
{
	if (!fullHistory.size())
	{
		emit changeStatus("Empty game. Load or start new");
		return ;
	}
	if (moveIndex < 0 || moveIndex >= fullHistory.size())
		currentIndex = fullHistory.size() - 1;
	else
		currentIndex = moveIndex;
	updateGameStatus();
	if (currentIndex == 0)
		emit changeStatus("First move");
	else if (currentIndex == fullHistory.size() - 1)
		emit changeStatus("Last move");
	figures = fullHistory[currentIndex];
	currentPlayer = (currentIndex % 2) ? Color::Black : Color::White;
	emit setCurrentPlayer(-1);
	for(int i = 0; i < figures.size(); i++)
		emit dataChanged(createIndex(i, 0), createIndex(i, 0));
}

/* New game: clearing history, prepating journal file and run startGame() */
void	GameLogic::newGame(void)
{
	fullHistory.clear();
	currentIndex = 0;
	if (openFile(CURRENTGAMEFILE, QIODevice::WriteOnly | QIODevice::Text))
		emit changeStatus("Started new game");
	startGame();
	fullHistory << figures;
}

/* Continue playing from current game - cut unnecessary moves at the end of journal and continue */
void	GameLogic::continueGame(void)
{
	unsigned int	bytes = 0;

	if (fullHistory.empty())
	{
		emit changeStatus("Empty game. Load or start new");
		return ;
	}
	if (currentIndex != fullHistory.size() - 1)
	{
		openFile(CURRENTGAMEFILE, QIODevice::ReadWrite | QIODevice::Text);
		for (int i = 0; i < currentIndex; i++)
		{
			QByteArray line = file.readLine();
			bytes += line.size();

			char	c;
			file.getChar(&c);
			file.ungetChar(c);
			if (c == 'P')
			{
				line = file.readLine();
				bytes += line.size();
			}
		}
		file.resize(bytes);
		while (fullHistory.size() - 1 > currentIndex)
			fullHistory.pop_back();
		currentIndex = fullHistory.size() - 1;
		figures = fullHistory[currentIndex];
		if (currentIndex > 0)
			checkEnPassantRule();
		updateMoves();
	}
	currentPlayer = (currentIndex % 2) ? Color::Black : Color::White;
	emit setCurrentPlayer(currentPlayer);
	for(int i = 0; i < figures.size(); i++)
		emit dataChanged(createIndex(i, 0), createIndex(i, 0));
}

/* Check if enPassant rule can be performed during this turn */
void	GameLogic::checkEnPassantRule(void)
{
	QList<Figure>	temp = fullHistory[currentIndex - 1];

	for (int i = 0; i < figures.size(); i++)
		if (temp[i].made_moves != figures[i].made_moves && figures[i].type == Type::Pawn)
			enPassantMove(&temp[i], figures[i].X, figures[i].Y);
}

/* Load history file, reset model */
bool	GameLogic::loadHistoryFile(QString filename)
{
	beginResetModel();
	if (!loadHistory(filename))
	{
		fullHistory.clear();
		figures.clear();
		endResetModel();
		return (false);
	}
	endResetModel();
	return (true);
}

/* Loading saved game becomes combination of loading history and continuing from current point */
void	GameLogic::loadGame(QString filename)
{
	if (!loadHistoryFile(filename))
		return ;
	currentIndex = fullHistory.size() - 1;
	figures = fullHistory[currentIndex];
	continueGame();
}

/* Add move to journal (aka history file) */
void	GameLogic::writeMove(int fromX, int fromY, int toX, int toY)
{
	char	letters[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};

	fullHistory << figures;
	currentIndex++;
	QTextStream	out(&file);
	out << letters[fromX] << abs(BOARD_CELLS - fromY) << ':' << letters[toX] << abs(BOARD_CELLS - toY) << '\n';
}

/* Write pawn exchange operation to journal(history file) */
void	GameLogic::writePawnChange(QString exchangeTo)
{
	fullHistory.last() = figures;
	QTextStream	out(&file);
	out << "Pawn:" << exchangeTo << '\n';
}

/* Saving game */
void	GameLogic::saveGame(QString filename)
{
	bool	result = true;
	QFile	destFile(filename);


	if (destFile.exists() && !destFile.remove())
		result = false;
	else if (!QFile::copy(CURRENTGAMEFILE, filename))
		result = false;
	emit changeStatus((result) ? "Game saved" : "Failed to save game");
}

/* Useful tools */
void	GameLogic::clearBoard(void)
{
	if(!fullHistory.empty())
	{
		beginResetModel();
		figures.clear();
		fullHistory.clear();
		endResetModel();
	}
}

bool	GameLogic::isEmpty(void)
{
	return (fullHistory.empty());
}

/* Open file: close current and open with given mask */
bool	GameLogic::openFile(QString filename, QFile::OpenMode mode)
{
	QDir::setCurrent(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
	file.close();
	file.setFileName(filename);
	if(!file.open(mode))
	{
		emit changeStatus("Error while opening file");
		return (false);
	}
	return (true);
}
